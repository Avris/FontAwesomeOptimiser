<?php

namespace Avris\FontawesomeOptimiser;

final class Optimiser
{
    const SETS = [
        'b' => 'brands',
        'd' => 'duotone',
        'l' => 'light',
        'r' => 'regular',
        's' => 'solid',
    ];

    /** @var string */
    private $spritesDir;

    /** @var string */
    private $defaultSet;

    /** @var string[][] */
    private $sprites = [];

    /** @var string[] */
    private $used = [];

    public function __construct(string $spritesDir, string $defaultSet = 'regular')
    {
        $this->spritesDir = $spritesDir;
        $this->defaultSet = $defaultSet;
    }

    public function icon(string $name, ?string $set = null): string
    {
        if (!$set) {
            $parts = explode(' ', $name);
            if (count($parts) > 1) {
                $set = $parts[0];
                $name = $parts[1];
            }
        }
        if (!$set) {
            $set = $this->defaultSet;
        }
        if (substr($set, 0, 2) === 'fa') {
            $set = self::SETS[substr($set, 2)];
        }

        return '<svg class="icon"><use xlink:href="#' . $this->loadIcon($name, $set) . '"></use></svg>';
    }

    public function iconSprites(): string
    {
        return '<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">' . join('', $this->used) . '</svg>';
    }

    private function loadIcon(string $name, string $set)
    {
        if (!isset($this->used[$set . '-' . $name])) {
            $this->loadSprites($set);

            if (!isset($this->sprites[$set][$name])) {
                throw new \RuntimeException(sprintf(
                    'Icon `%s` (`%s`) not found',
                    $name,
                    $set
                ));
            }

            $this->used[$set . '-' . $name] = $this->sprites[$set][$name];
        }

        return $set . '-' . $name;
    }

    private function loadSprites(string $set)
    {
        if (!isset($this->sprites[$set])) {
            $xml = file_get_contents($this->spritesDir . '/' . $set . '.svg');

            preg_match_all('#<symbol id="(.*)" .*>.*<\/symbol>#Uuims', $xml, $matches, PREG_SET_ORDER);

            $icons = [];
            foreach ($matches as list($symbol, $icon)) {
                $icons[$icon] = preg_replace(
                    '#>\s*<#Uuims',
                    '><',
                    str_replace('"' . $icon . '"', '"' . $set . '-' . $icon . '"', $symbol)
                );
            }

            $this->sprites[$set] = $icons;
        }
    }

    public function copy(): self
    {
        $clone = clone $this;
        $clone->used = [];

        return $clone;
    }
}
