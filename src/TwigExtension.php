<?php

namespace Avris\FontawesomeOptimiser;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class TwigExtension extends AbstractExtension
{
    /** @var Optimiser */
    private $optimiser;

    public function __construct(Optimiser $optimiser)
    {
        $this->optimiser = $optimiser;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('icon', [$this->optimiser, 'icon'], ['is_safe' => ['html']]),
            new TwigFunction('iconSprites', [$this->optimiser, 'iconSprites'], ['is_safe' => ['html']]),
        ];
    }
}
